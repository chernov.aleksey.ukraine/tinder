<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="img/favicon.ico">

    <title>People list</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <!-- Bootstrap core CSS -->
            <link rel="stylesheet" href="assets/css/bootstrap.min.css" >

        <!-- Custom styles for this template -->
        <link rel="stylesheet" href="assets/css/style.css">
</head>



<body>

<div class="container">
    <div class="row">
        <div class="col-8 offset-2">
            <div class="panel panel-default user_panel">
                <div class="panel-heading">
                    <h3 class="panel-title">User List</h3>

                    <div class="panel-heading-spec">
                        <p class="arrow-up mb-0" style="margin-top:-4px; ">
                        <button title="Get Another Friends" onclick="window.location.href='/users'; return false" style="border: none; background-color: transparent;">
                             <i class="fa fa-arrow-up text-center pt-1" style="transform: rotate(45deg);"></i>
                        </button>
                        </p>

                        <button title="LogOut" onclick="window.location.href='/logout'; return false" style="border: none; background-color: transparent;">
                            <i class="fa fa-cog"></i>
                        </button>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="table-container">
                        <table class="table-users table" border="0">
                            <tbody>
                            <#list likes as like>
                            <#if like??>
                                <tr onclick="window.location.href='/message?id=${like.id}'; return false">
                                    <td width="10">
                                        <div class="avatar-img">
                                            <img class="img-circle" src=${like.imgUrl} />
                                        </div>
                                    </td>
                                    <td class="align-middle">
                                        ${like.fullname}
                                    </td>
                                    <td class="align-middle">
                                        ${like.job}
                                    </td>
                                    <td  class="align-middle">
                                        Last Login:  ${like.lastLogining} <br><small class="text-muted">${like.days} days ago</small>
                                    </td>
                                </tr>
                            </#if>
                            </#list>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>




</html>