package org.example;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.example.controllers.*;
import org.example.dao.JdbcLikeDao;
import org.example.dao.JdbcMessageDao;
import org.example.dao.JdbcUserDao;
import org.example.dao.UserDao;
import org.example.domain.TemplateEngine;
import org.example.service.*;

import javax.servlet.DispatcherType;
import javax.servlet.http.Cookie;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpHeaders;
import java.net.http.HttpRequest;
import java.time.Duration;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.Optional;

import static org.example.controllers.LoginFilter.USER_PARAM_ID;


public class JettyRun {
    public static void main(String[] args) throws Exception {

//        Integer port = ;

        TemplateEngine templateEngine = new TemplateEngine();
        Server server = new Server(Integer.parseInt(System.getenv("PORT") == null?"8081":System.getenv("PORT")));
        ServletContextHandler handler = new ServletContextHandler();



        JdbcMessageDao jdbcMessageDao = new JdbcMessageDao();
        DefaultMessageService messageService = new DefaultMessageService(jdbcMessageDao);
        JdbcLikeDao likedDao = new JdbcLikeDao();
        DefaultLikeService likeService = new DefaultLikeService(likedDao);
        UserDao userDao = new JdbcUserDao();
        DefaultUserService userService = new DefaultUserService((JdbcUserDao) userDao);

        LoginServlet loginServlet = new LoginServlet(userService, templateEngine);
        UserServlet userServlet = new UserServlet(templateEngine, userService, likeService );
        LikedServlet likedServlet = new LikedServlet(templateEngine,userService,likeService);
        MessagesServlet messagesServlet = new MessagesServlet(templateEngine,messageService,userService);
        LogOutServlet logOutServlet = new LogOutServlet(templateEngine);

        handler.addServlet(new ServletHolder(loginServlet), "/");
        handler.addServlet(new ServletHolder(userServlet), "/users");
        handler.addServlet(new ServletHolder(likedServlet), "/liked");
        handler.addServlet(new ServletHolder(messagesServlet), "/message");
        handler.addServlet(new ServletHolder(logOutServlet), "/logout");
        handler.addServlet(new ServletHolder(new FileServlet()), "/assets/*");
//        handler.addServlet(CssBootstrapServlet.class, "/assets/css/bootstrap.min.css");

        handler.addFilter(new FilterHolder(new LoginFilter(templateEngine, userService)), "/users", EnumSet.of(DispatcherType.INCLUDE, DispatcherType.REQUEST));
        handler.addFilter(new FilterHolder(new LoginFilter(templateEngine, userService)), "/liked", EnumSet.of(DispatcherType.INCLUDE, DispatcherType.REQUEST));
        handler.addFilter(new FilterHolder(new LoginFilter(templateEngine, userService)), "/message", EnumSet.of(DispatcherType.INCLUDE, DispatcherType.REQUEST));

        server.setHandler(handler);
        server.start();
        server.join();

    }
// handler.addServlet(new ServletHolder(new RegistrationServlet(defaultProfileService, templateEngine)), "/registration");
//
}