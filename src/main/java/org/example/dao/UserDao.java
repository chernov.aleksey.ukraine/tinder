package org.example.dao;
import org.example.domain.User;
import org.postgresql.ds.PGPoolingDataSource;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

public interface UserDao {
    User read(Long id);
    public User readForLiked(Long idParam);
    void update(Long userId);

    boolean delete(long id);
    List<User> findAll();
    User findByLoginPass(String login,String password);


//    default Connection getConnection() throws SQLException {
default Connection  getConnection()throws SQLException {

//         return DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/postgres", "postgres","postgres");
 return DriverManager.getConnection("jdbc:postgresql://snuffleupagus.db.elephantsql.com:5432/fzfxxhng", "fzfxxhng", "r2kMXKOg9XSVFuv71GaXVo1X7Gae0WjX");

    }
}
