package org.example.dao;

import org.example.domain.Like;
import org.example.domain.Message;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JdbcMessageDao implements MessageDao{
    @Override
    public List<Message> findAll() {
        List <Message> currantMessages = new ArrayList<>();
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM public.message ");
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Long sender = rs.getLong(2);
                Long receiver = rs.getLong(3);
                String text = rs.getString(4);
                String date = rs.getString(5);

                currantMessages.add( new Message( sender, receiver, text, date ));}
        } catch (SQLException e) {
            e.printStackTrace();}
        return currantMessages;

    }

    @Override
    public List<Message> readByParticipantsIds(Long senderid, Long receiverid) {
        List <Message> currantMessages = new ArrayList<>();
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM public.message WHERE (sender=? AND receiver=?) OR ( sender=? AND receiver=?)");
            statement.setLong(1, senderid);
            statement.setLong(2, receiverid);
            statement.setLong(3, receiverid);
            statement.setLong(4, senderid);

            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Long sender = rs.getLong(2);
                Long receiver = rs.getLong(3);
                String text = rs.getString(4);
                String date = rs.getString(5);

                currantMessages.add( new Message( sender, receiver, text, date ));}
        } catch (SQLException e) {
            e.printStackTrace();}
        return currantMessages;

    }

    @Override
    public void addMessage(Long id, Long senderid, Long receiverid, String text, String dataTime) {
        System.out.println("dao try to add message ");
        try (Connection connection = getConnection()) {
            System.out.println("connection for adding");
            PreparedStatement statement = connection.prepareStatement("INSERT INTO public.message (id,sender,receiver, content, time) VALUES(?,?,?, ?,?)");
            statement.setLong(1, id);
            statement.setLong(2, senderid);
            statement.setLong(3, receiverid);
            statement.setString(4, text);
            statement.setString(5, dataTime);
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();}
    }
}
