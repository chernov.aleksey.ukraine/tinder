package org.example.dao;

import org.example.domain.User;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class JdbcUserDao implements UserDao{



    @Override
    public User read(Long idParam) {

        try (Connection connection = getConnection()) {

            PreparedStatement statement = connection.prepareStatement("SELECT * FROM public.user WHERE id=?");
            statement.setLong(1, idParam);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                Long id = rs.getLong(1);
                String name = rs.getString(2);
                String surname = rs.getString(3);
                String photo = rs.getString(4);
                String login = rs.getString(5);
                String password = rs.getString(6);
                String job = rs.getString(7);
                String lastLogining = rs.getString(8);

                return new User( id,  name,  surname,  photo, login,  password,  job,  lastLogining);
            }
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }
    @Override
    public User readForLiked(Long idParam) {

        try (Connection connection = getConnection()) {

            PreparedStatement statement = connection.prepareStatement("SELECT * FROM public.user WHERE id=?");
            statement.setLong(1, idParam);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                Long id = rs.getLong(1);
                String name = rs.getString(2);
                String surname = rs.getString(3);
                String photo = rs.getString(4);
                String job = rs.getString(7);
                String lastLogining = rs.getString(8);
                String fullname = name + " " + surname;
                String days =  String.valueOf((new Date().getTime() - LocalDate.parse(lastLogining, DateTimeFormatter.ofPattern("dd/MM/yyyy")).atStartOfDay(ZoneOffset.UTC).toInstant().toEpochMilli())/86400000);
                return new User( id,  fullname,  photo, job,  lastLogining, days);
            }
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void update(Long  id) {
        LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/YYYY"));
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection.prepareStatement("UPDATE public.user SET lastlogining=? WHERE id=?");
            statement.setString(1, LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/YYYY")));
            statement.setLong(2, id);
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();}
    }

    @Override
    public boolean delete(long id) {
        return false;
    }

    @Override
    public List<User> findAll() {
        List <User> users = new ArrayList<>();
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM public.user ");

            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Long id = rs.getLong(1);
                String login = rs.getString(5);
                String password = rs.getString(6);
                users.add( new User(id,login,password));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    @Override
    public User findByLoginPass(String loginParam, String passwordParam) {

        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM public.user WHERE  login=? AND password=?");
            statement.setString(1, loginParam);
            statement.setString(2, passwordParam);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                Long id = rs.getLong(1);
                String login = rs.getString(5);
                String password = rs.getString(6);

                return new User(id,login,password);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }



}

