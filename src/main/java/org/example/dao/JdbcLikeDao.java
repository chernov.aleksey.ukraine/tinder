package org.example.dao;

import org.example.domain.Like;
import org.example.domain.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JdbcLikeDao implements LikeDao{
    @Override
    public List<Like> readBySetterId(Long id) {
        List <Like> currantLikes = new ArrayList<>();
        try (Connection connection = getConnection()) {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM public.like WHERE setterid=? ");
            statement.setLong(1, id);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Long setter = rs.getLong(2);
                Long receiver = rs.getLong(3);
                currantLikes.add( new Like( setter, receiver ));}
        } catch (SQLException e) {
            e.printStackTrace();}
        return currantLikes;}

    @Override
    public void addLike(Long id, Long setterId, Long receiverId) {
        System.out.println("dao try to add like ");
        try (Connection connection = getConnection()) {
            System.out.println("connection for adding");
            PreparedStatement statement = connection.prepareStatement("INSERT INTO public.like (id,setterid,receiverid) VALUES(?,?,?)");
            statement.setLong(1, id);
            statement.setLong(2, setterId);
            statement.setLong(3, receiverId);
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();}

    }

    @Override
    public void deleteAllLikes() {
        System.out.println("dao try to delete");
        try (Connection connection = getConnection()) {
            System.out.println("connection for delete");
            PreparedStatement statement = connection.prepareStatement("DELETE FROM public.like ");
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();}
    }



}
