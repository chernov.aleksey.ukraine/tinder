package org.example.domain;

public class Message {
    Long senderId;
    Long receiverId;
    String text;
    String dataTime;

    public Long getSenderId() {return senderId;}
    public void setSenderId(Long senderId) {this.senderId = senderId;}
    public Long getReceiverId() {return receiverId;}
    public void setReceiverId(Long receiverId) {this.receiverId = receiverId;}
    public String getText() {return text;}
    public void setText(String text) {this.text = text;}
    public String getDataTime() {return dataTime;}
    public void setDataTime(String dataTime) {this.dataTime = dataTime;}

    public Message(Long senderId, Long receiverId, String text, String dataTime ) {
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.text = text;
        this.dataTime = dataTime;}

    @Override
    public String toString() {return "Message{" + "senderId=" + senderId + ", receiverId=" + receiverId
    + ", text=" + text + ", dataTime=" + dataTime + "}";}
}
