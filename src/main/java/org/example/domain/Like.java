package org.example.domain;

public class Like {
    Long setterId;
    Long receiverId;

    public Long getSetterId() {return setterId;}
    public void setSetterId(Long setterId) {this.setterId = setterId;}
    public Long getReceiverId() {return receiverId;}
    public void setReceiverId(Long receiverId) {this.receiverId = receiverId;}

    public Like(Long setterId, Long receiverId) {
        this.setterId = setterId;
        this.receiverId = receiverId;}

    @Override
    public String toString() {return "Like{" + "setterId=" + setterId + ", receiverId=" + receiverId + "}";}
}
