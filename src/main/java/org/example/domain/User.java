package org.example.domain;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class User {
    private Long id;
    private String login;
    private String password;
    private String name;
    private String surname;


    private String fullname;
    private String imgUrl;
    private String job;
    private String lastLogining;
    private String days;

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}
    public String getLogin() {return login;}
    public void setLogin(String login) {this.login = login;}
    public String getPassword() {return password;}
    public void setPassword(String password) {this.password = password;}
    public String getName() {return name;}
    public void setName(String name) {this.name = name;}
    public String getSurname() {return surname;}
    public void setSurname(String surname) {this.surname = surname;}
    public String getImgUrl() {return imgUrl;}
    public void setImgUrl(String imgUrl) {this.imgUrl = imgUrl;}
    public String getJob() {return job;}
    public void setJob(String job) {this.job = job;}
    public String getLastLogining() {return lastLogining;}
    public void setLastLogining(String lastLogining) {this.lastLogining = lastLogining;}
    public String getFullname() {return fullname;}
    public void setFullname(String fullname) {this.fullname = fullname;}
    public String getDays() {return days;}
    public void setDays(String days) {this.days = days;}

    public User(Long id,String login, String password) {
        this.id = id;
        this.login = login;
        this.password = password;}

    public User(Long id, String name, String surname, String imgUrl, String login, String password, String job, String lastLogining) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.imgUrl = imgUrl;
        this.login = login;
        this.password = password;
        this.job = job;
        this.lastLogining = lastLogining;}

    public User(Long id, String fullname, String imgUrl, String job, String lastLogining, String days) {
        this.id = id;
        this.fullname = fullname;
        this.imgUrl = imgUrl;
        this.job = job;
        this.lastLogining = lastLogining;
        this.days = days;
    }
    @Override
    public String toString() {return "User{id=" + id + ", name=" + name + ", surname='" + surname + ", imgUrl="
    + imgUrl +  ", login=" + login + ", password=" + password +  ", job=" + job +  ", lastLogining=" + lastLogining+"}";}
}

