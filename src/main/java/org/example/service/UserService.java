package org.example.service;

import org.example.domain.User;

import java.io.InputStream;
import java.util.List;

public interface UserService {
//    boolean create(User user);
    User read(Long id);
    void update(Long userId);
    public User readForLiked(Long idParam);
    List<User> findAll();
    User findByLoginPass(String login,String password);
    User findById(Long userId);

}
