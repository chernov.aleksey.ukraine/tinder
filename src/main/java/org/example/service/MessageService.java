package org.example.service;

import org.example.domain.Message;

import java.util.List;

public interface MessageService {
    List<Message> findAll();
    List<Message> readByParticipantsIds(Long senderid, Long receiverid);
    void addMessage(Long id, Long setterid, Long receiverid, String text, String dataTime);
}
