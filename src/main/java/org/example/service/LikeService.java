package org.example.service;

import org.example.domain.Like;

import java.util.List;

public interface LikeService {
    List<Like> readBySetterId(Long id);
    void addLike(Long id, Long setterid, Long receiverid);
    void deleteAllLikes();
}
