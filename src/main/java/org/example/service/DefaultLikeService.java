package org.example.service;

import org.example.dao.JdbcLikeDao;
import org.example.dao.JdbcUserDao;
import org.example.domain.Like;

import java.util.List;

public class DefaultLikeService implements LikeService{
    private JdbcLikeDao likeDao;

    public DefaultLikeService(JdbcLikeDao likeDao) {
        this.likeDao = likeDao;
    }

    @Override
    public List<Like> readBySetterId(Long id) {
        return this.likeDao.readBySetterId( id);
    }

    @Override
    public void addLike(Long id, Long setterid, Long receiverid) {
        System.out.println("Likeservice for adding");
    this.likeDao.addLike(id, setterid, receiverid);
    }

    @Override
    public void deleteAllLikes() {
        System.out.println("Likeservice for delete");
        this.likeDao.deleteAllLikes();
    }
}
