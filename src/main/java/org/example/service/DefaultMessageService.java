package org.example.service;

import org.example.dao.JdbcMessageDao;
import org.example.domain.Message;

import java.util.List;

public class DefaultMessageService implements MessageService{
    private JdbcMessageDao messageDao;

    public DefaultMessageService(JdbcMessageDao messageDao){this.messageDao = messageDao; }


    @Override
    public List<Message> findAll() {
        return this.messageDao.findAll();
    }

    @Override
    public List<Message> readByParticipantsIds(Long senderid, Long receiverid) {

        return this.messageDao.readByParticipantsIds(senderid,receiverid);}

    @Override
    public void addMessage(Long id, Long setterid, Long receiverid, String text, String dataTime) {
       this.messageDao.addMessage( id,  setterid,  receiverid,  text,  dataTime);
    }
}
