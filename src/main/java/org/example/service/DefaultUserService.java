package org.example.service;
import org.example.dao.JdbcUserDao;
import org.example.dao.UserDao;
import org.example.domain.User;

import java.io.InputStream;
import java.util.List;


    public class DefaultUserService implements UserService {
        private JdbcUserDao userDao;

        public DefaultUserService(JdbcUserDao userDao) {
            this.userDao = userDao;
        }



        @Override
        public User read(Long id) {

            return userDao.read(id);
        }
        public User readForLiked(Long idParam){return this.userDao.readForLiked(idParam);}
        @Override
        public void update(Long userId) {
            userDao.update(userId);
        }



        @Override
        public List<User> findAll() {
            return userDao.findAll();
        }

        @Override
        public User findByLoginPass(String login, String password) {
            return userDao.findByLoginPass(login, password);
        }



        @Override
        public User findById(Long userId) {
            return userDao.read(userId);
        }
    }


