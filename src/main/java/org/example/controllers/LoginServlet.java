package org.example.controllers;

import org.example.domain.TemplateEngine;
import org.example.service.CookieUtil;
import org.example.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.example.controllers.LoginFilter.USER_PARAM_ID;
import static org.example.controllers.LoginFilter.globalUserId;

public class LoginServlet extends HttpServlet {
    private UserService userService;
    private TemplateEngine templateEngine;



    public LoginServlet(UserService userService, TemplateEngine templateEngine) {
        this.userService = userService;
        this.templateEngine = templateEngine;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println(request.getServletPath());
        if(!CookieUtil.findCookieByName(request, USER_PARAM_ID).isEmpty() && request.getServletPath().equals("/")){response.sendRedirect("/users");}
        templateEngine.render("index.ftl", response);
    }
}