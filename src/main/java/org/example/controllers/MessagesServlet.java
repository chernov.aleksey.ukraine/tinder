package org.example.controllers;

import org.example.domain.Message;
import org.example.domain.TemplateEngine;
import org.example.domain.User;
import org.example.service.DefaultMessageService;
import org.example.service.DefaultUserService;
import org.example.service.MessageService;
import org.example.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static org.example.controllers.LoginFilter.globalUserId;


public class MessagesServlet extends HttpServlet {
    TemplateEngine templateEngine;
    MessageService messageService;
    UserService userService;
    static Long globalLikedId =0L;
    String defaultMessage ="";
    String [] month = {"Jan.", "Feb.", "Mar.", "Apr.", "May.", "Jun.", "Jul.", "Aug.", "Sep.", "Oct.", "Nov.", "Dec."};
    String DataForSend;
    public MessagesServlet(TemplateEngine templateEngine, MessageService messageService, UserService userService) {
        this.templateEngine = templateEngine;
        this.messageService = messageService;
        this.userService = userService;}

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        globalLikedId = Long.valueOf(req.getParameter("id"));
            User mainUser = userService.findById(globalUserId);
            User currantUser = userService.findById(globalLikedId);
            List<Message> currantMessages = messageService.readByParticipantsIds(globalUserId, globalLikedId);
            if (currantMessages.size()==0){defaultMessage = "THERE IS NO MESSAGES THERE. TRY TO WRITE FIRST))))!";
            }else{defaultMessage = "";}
        Map<String, Object> params = Map.of(
        "data", List.of(globalUserId, globalLikedId),
        "messages", currantMessages,
        "names", List.of(mainUser.getName(), mainUser.getSurname(), mainUser.getImgUrl(),
        currantUser.getName(), currantUser.getSurname(),currantUser.getImgUrl()),
        "default", List.of(defaultMessage));
        templateEngine.render("message.ftl", params, resp);
    }
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String text = req.getParameter("new_message");
        Long newMessageId = Long.valueOf(messageService.findAll().size()+1);
        DataForSend = month[Integer.parseInt( LocalDateTime.now().format(DateTimeFormatter.ofPattern("MM")))-1]+
                " " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd, h:mm a"));
        messageService.addMessage(newMessageId, globalUserId,globalLikedId,text, DataForSend);
        resp.sendRedirect("/message?id=" + globalLikedId);
    }
}

