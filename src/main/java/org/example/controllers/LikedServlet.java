package org.example.controllers;

import org.example.domain.TemplateEngine;
import org.example.domain.User;
import org.example.service.LikeService;
import org.example.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static org.example.controllers.LoginFilter.globalUserId;

public class LikedServlet extends HttpServlet {
        public TemplateEngine templateEngine;
        LikeService likeService;
        UserService userService;
    List<User> likedUsers;
        public LikedServlet(TemplateEngine templateEngine, UserService userService, LikeService likeService) {
            this.templateEngine = templateEngine;
            this.likeService = likeService;
            this.userService = userService;}
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

                 likedUsers = new ArrayList<User>();

            likeService.readBySetterId(globalUserId).stream().forEach(e -> {
                likedUsers.add(userService.readForLiked(e.getReceiverId()));});

            Map<String, Object> params = Map.of("likes",  likedUsers );
            templateEngine.render("liked.ftl", params, resp);}
        protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            resp.sendRedirect("/users");
    }
}
