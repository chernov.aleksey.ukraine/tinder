package org.example.controllers;

import org.example.domain.TemplateEngine;
import org.example.domain.User;
import org.example.service.DefaultUserService;
import org.example.service.LikeService;
import org.example.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;

import static org.example.controllers.LoginFilter.USER_PARAM_ID;
import static org.example.controllers.LoginFilter.globalUserId;
public class UserServlet extends HttpServlet {
    public TemplateEngine templateEngine;
    public UserService userService;
    public LikeService likeService;
    public UserServlet(TemplateEngine templateEngine, UserService userService, LikeService likeService) {
        this.templateEngine = templateEngine;
        this.likeService = likeService;
        this.userService = userService;}
    public static Long count =0L;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        this.doPost(req,resp);}
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("count is " + count);
        System.out.println("Param1 " + req.getParameter("param1"));
        if(req.getParameter("param1") == null){count = 0L;}
        if(count==0 ){
            System.out.println("servlet ask delete");
            likeService.deleteAllLikes();
        }else{
            System.out.println(req.getParameter("param1"));
            if(req.getParameter("param1").equals("YES")){
                likeService.addLike(count, globalUserId, count);
                System.out.println("servlet ask add like");
            }
        }


        count++;
        if (count == globalUserId){count++;}
        if (count >6L){
                count = 0L;
                resp.sendRedirect("/liked");
                return;}
        Map<String, Object> params = Map.of("profiles",   userService.read(count), "currant", userService.read(globalUserId));
        templateEngine.render("users.ftl", params, resp);
        System.out.println("user servlet " +  globalUserId );}
}
