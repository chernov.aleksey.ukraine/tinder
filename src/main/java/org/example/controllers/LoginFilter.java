package org.example.controllers;

import org.eclipse.jetty.server.Authentication;
import org.example.domain.TemplateEngine;
import org.example.domain.User;
import org.example.service.CookieUtil;
import org.example.service.DefaultUserService;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Optional;
import java.util.Set;

public class LoginFilter implements Filter {
    public static final String USER_PARAM_ID = "userId";
    static Long globalUserId = 0L;
    private TemplateEngine templateEngine;
    private DefaultUserService  userService;
    public LoginFilter(TemplateEngine templateEngine, DefaultUserService userService) {
        this.templateEngine = templateEngine;
        this.userService = userService;
    }
    public void init(FilterConfig config) throws ServletException {}
    public void destroy() {}
    @Override
    public void doFilter(ServletRequest req, ServletResponse resp,
                         FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        System.out.println(request.getServletPath());
        Set<String> allowedUrls = Set.of("/registration", "/assets");
        if (allowedUrls.contains(request.getServletPath())) {chain.doFilter(request, response);}
        Optional<Cookie> optionalCookie = CookieUtil.findCookieByName(request, USER_PARAM_ID);
        if (optionalCookie.isEmpty()) {
            String login = request.getParameter("login");
            String password = request.getParameter("password");
            User user = userService.findByLoginPass(login, password);
            if (user != null) {
                globalUserId = user.getId();
                userService.update(globalUserId);
                response.addCookie(new Cookie(USER_PARAM_ID, String.valueOf(user.getId())));
                chain.doFilter(request, response);}
            templateEngine.render("index.ftl", new HashMap<>(), response);
        } else {globalUserId = Long.valueOf(optionalCookie.get().getValue());
            chain.doFilter(request, response);}}
}

