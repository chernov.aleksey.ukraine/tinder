package org.example.controllers;

import org.example.domain.TemplateEngine;
import org.example.service.CookieUtil;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

import static org.example.controllers.LoginFilter.USER_PARAM_ID;
import static org.example.controllers.UserServlet.count;

public class LogOutServlet extends HttpServlet {
    private TemplateEngine templateEngine;

    public LogOutServlet(TemplateEngine templateEngine) {
        this.templateEngine = templateEngine;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        count = 0L;
        CookieUtil.findCookieByName(request, USER_PARAM_ID)
                .ifPresent(c -> {
                    c.setMaxAge(0);
                    c.setPath("/");
                    response.addCookie(c);
                });

        templateEngine.render("index.ftl", response);
    }
}